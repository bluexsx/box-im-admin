package org.dromara.im.constant;

/**
 * @author: Blue
 * @date: 2024-07-20
 * @version: 1.0
 */
public class ImConstant {

    /**
     *  IM数据源
     */
    public final static String DS_IM_PLATFORM = "platform";
}
