package org.dromara.im.constant;

public class ImRedisKey {

    /**
     * 缓存群聊信息
     */
    public static final String IM_CACHE_GROUP =  "im:cache:group";
    /**
     * 用户被封禁处理队列
     */
    public static final String IM_QUEUE_USER_BANNED = "im:queue:user:banned";

    /**
     * 群聊被封禁处理队列
     */
    public static final String IM_QUEUE_GROUP_BANNED = "im:queue:group:banned";

    /**
     * 群聊解封处理队列
     */
    public static final String IM_QUEUE_GROUP_UNBAN = "im:queue:group:unban";




}
