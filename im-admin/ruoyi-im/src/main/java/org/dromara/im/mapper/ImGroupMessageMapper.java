package org.dromara.im.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.im.domain.ImGroupMessage;
import org.dromara.im.domain.vo.ImGroupMessageVo;

/**
 * 群消息Mapper接口
 *
 * @author Blue
 * @date 2024-12-22
 */
public interface ImGroupMessageMapper extends BaseMapperPlus<ImGroupMessage, ImGroupMessageVo> {

}
