package org.dromara.im.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.im.domain.ImPrivateMessage;
import org.dromara.im.domain.vo.ImPrivateMessageVo;

/**
 * 私聊消息Mapper接口
 *
 * @author Blue
 * @date 2024-12-22
 */
public interface ImPrivateMessageMapper extends BaseMapperPlus<ImPrivateMessage, ImPrivateMessageVo> {

}
