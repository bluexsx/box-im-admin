package org.dromara.im.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.im.domain.ImSensitiveWord;
import org.dromara.im.domain.vo.ImSensitiveWordVo;

/**
 * 敏感词Mapper接口
 *
 * @author Blue
 * @date 2024-12-22
 */
public interface ImSensitiveWordMapper extends BaseMapperPlus<ImSensitiveWord, ImSensitiveWordVo> {

}
