package org.dromara.im.mapper;

import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.im.domain.ImUser;
import org.dromara.im.domain.vo.ImUserVo;

/**
 * 用户Mapper接口
 *
 * @author Blue
 * @date 2024-12-22
 */
public interface ImUserMapper extends BaseMapperPlus<ImUser, ImUserVo> {

}
