package org.dromara.im.mq;

import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author: Blue
 * @date: 2024-07-16
 * @version: 1.0
 */
public class ImRedisMQTemplate extends RedisTemplate<String,Object> {

}
